# -*- coding: utf-8 -*-

# 导入需要的库
import tkinter as tk
from tkinter import filedialog

# 定义指令与二进制代码的对应关系
CODE_MAP = {
    'setM1': '1000',
    'setR': '1001',
    'readM1': '1010',
    'add': '0100',
    'sub': '0101',
    'mul': '0110',
    'exc': '0111',
    'loc': '0010',
    'jup': '0011'
}

# 定义主窗口
root = tk.Tk()
root.title('Assembler')
root.geometry('400x200')

# 定义文件选择按钮
file_button = tk.Button(root, text='选择文件', command=lambda: select_file())
file_button.pack()

# 定义输出文本框
output_text = tk.Text(root, width=400, height=150)
output_text.pack()

# 定义文件选择函数
def select_file():
    file_path = filedialog.askopenfilename()
    if file_path:
        # 读取文件内容
        with open(file_path, 'r') as f:
            lines = f.readlines()
        
        # 遍历文件内容，替换指令为二进制代码
        for line in lines:
            for code in CODE_MAP:
                if code in line:
                    line = line.replace(code, CODE_MAP[code])
            output_text.insert('end', line)

# 启动程序
root.mainloop()
